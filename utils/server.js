var express = require('express');
var errorHandler = require('errorhandler');
var bodyParser = require('body-parser');

var app = module.exports = express();

app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//  static files
app.use(express.static('src'));

// error handling middleware should be loaded after the loading the routes
if ('development' == app.get('env')) {
    app.use(errorHandler());
}

app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});