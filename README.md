## Install
* Run `npm install` for the server dependencies
* Run `bower install` for the client dependencies.
* Type `npm start` to get going.

## License
MIT
