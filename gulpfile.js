var gulp = require('gulp');
var gulpFilter = require('gulp-filter');
var mainBowerFiles = require('main-bower-files');
var beautify = require('gulp-beautify');
var concat = require('gulp-concat');

gulp.task('movejs', function() {

    var jsFilter = gulpFilter(['angular.js', 'angular-ui-router.js']);

    // return gulp.src(mainBowerFiles())
    //     .pipe(jsFilter)
    //     .pipe(gulp.dest('./src/js'));
});

gulp.task('movecss', function() {

    return gulp.src(['./bower_components/bootstrap/dist/css/bootstrap.min.css'])
        .pipe(gulp.dest('./src/css'));

});

// gulp.task('beautify-js', function() {
//     return gulp.src(['**/*.js', '!./node_modules/**', '!./bower_components/**', '!./src/components/**', '!./src/css/**'], {
//             base: './'
//         })
//         .pipe(beautify())
//         .pipe(gulp.dest('./'));
// });

gulp.task('move', function() {
    gulp.start('movejs', 'movecss');
});
gulp.task('build', function() {
    gulp.start('concat');
});

gulp.task('concat', function() {

    return gulp.src(['./bower_components/angular/angular.min.js', './bower_components/angular-ui-router/release/angular-ui-router.min.js', './src/app/**/*.js'])
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('./src/'));
});
