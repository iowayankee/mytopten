(function () {
	'use strict';
	angular
		.module('topTenApp')
		.factory('toptenService', toptenService);

	toptenService.$inject = ['$http', '$q', 'dataService'];

	function toptenService($http, $q, dataService) {

		var plkey = 'nplss'; //	localstorage key for playlists

		var factory =  {
			initApp: initApp(),
			getPlaylist: getPlaylist,
			createPlaylist: createPlaylist,
			removePlaylist: removePlaylist,
			searchTunes: searchTunes,
			getTracks: getTracks,
			addToPlaylist: addToPlaylist,
			removeFromPlaylist: removeFromPlaylist
		};
		return factory;

 		function initApp() {
			loginCheck();
			var playlists = dataService.getObject(plkey)
			console.log('my playlists ' + plkey, playlists)

			return {playlists: playlists};
		}

		function createPlaylist(title) {
			var pl = dataService.getObject(plkey)
			pl.push({title: title, tracks: []})
			dataService.setObject(plkey, pl)
			return pl;
		}

		function removePlaylist(index) {
			var pl = dataService.getObject(plkey)
			var newPlaylists = [
				...pl.slice(0, index),
				...pl.slice(index + 1)
			];
			dataService.setObject(plkey, newPlaylists)
			return pl;
		}

		function getPlaylist(pid) {
			var pl = dataService.getObject(plkey)
			return pl[pid];
		}

		function addToPlaylist (pid, track, href) {
			var pl = dataService.getObject(plkey)
			pl[pid].tracks.push({track: track.name, artist: track.artists[0].name, href: href})
			dataService.setObject(plkey, pl)
			return pl;
		}

		function removeFromPlaylist(pid, index) {
			var pl = dataService.getObject(plkey)
			var newPlaylist = [
				...pl[pid].tracks.slice(0, index),
				...pl[pid].tracks.slice(index + 1)
			];
			pl[pid].tracks = newPlaylist;
			dataService.setObject(plkey, pl)
			return pl[pid].tracks;
		}

		function searchTunes(searchTerm, searchType){
			console.log('searchTunes', searchTerm, searchType)
			var str = "search?q=" + searchTerm + '&limit=5&type='+searchType;
			return dataService.getApiCall(str)
				.then(function(data) {
						return data;
					}, function(error) {
						console.log('error', error);
					});

		}

		function getTracks(albumurl){
			var albumid = albumurl.split('/');
			console.log('getTracks', albumid[albumid.length-1]);
			var str = "albums/" + albumid[albumid.length-1];
			return dataService.getApiCall(str)
				.then(function(data) {
						return data;
					}, function(error) {
						console.log('error', error);
					});

		}

		function loginCheck() {
			if(dataService.getLocal(plkey) == undefined) {
				console.log('loginCheck')
				dataService.setObject(plkey, [])
			}

		}

	}

})();
