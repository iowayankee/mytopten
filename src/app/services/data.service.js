(function () {
	'use strict';
	angular
		.module('topTenApp')
		.factory('dataService', dataService);

	dataService.$inject = ['$http', '$q', '$window'];

	function dataService($http, $q, $window) {

		var api_url = "https://api.spotify.com/v1/";

		var factory =  {
			getApiCall: getApiCall,
			getApiCall2: getApiCall2,
			setLocal: setLocal,
			getLocal: getLocal,
			setObject: setObject,
			getObject: getObject
		};
		return factory;

		function getApiCall2(searchTerm, method) {
			var url = api_url + "search?q=" + searchTerm + '&limit=10&type='+method;
			console.log('getApiCall: ' + url)

			return $http.get(url)
				.then(function (response) {
					console.log('api response', response.data)
					if (typeof response.data === 'object') {
						return response.data;
					} else {
						return $q.reject(response.data);
					}
				}, function (response) {
					return $q.reject(response.data);
				});

		}
		function getApiCall(url_string) {
			var url = api_url + url_string;
			console.log('getApiCall: ' + url)
			return $http.get(url)
				.then(function (response) {
					console.log('api response', response.data)
					if (typeof response.data === 'object') {
						return response.data;
					} else {
						return $q.reject(response.data);
					}
				}, function (response) {
					return $q.reject(response.data);
				});

		}

		function setLocal(key, value) {
			$window.localStorage[key] = value;
		}
		function getLocal(key, defaultValue) {
			return $window.localStorage[key] || defaultValue;
		}
		function setObject(key, value) {
			$window.localStorage[key] = JSON.stringify(value);
		}
		function getObject(key) {
			return JSON.parse($window.localStorage[key] || []);
		}

	}

})();
