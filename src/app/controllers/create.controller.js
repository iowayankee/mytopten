(function () {
    'use strict';

    angular
        .module('topTenApp')
        .controller('createController', createController);

    createController.$inject = ['toptenService', '$state'];

    function createController(toptenService, $state) {

        let vm = this;

        vm.toptenTitle = 'New awesome top 10 playlist'
        vm.toptens = toptenService.initApp;

        vm.createPlaylist = createPlaylist;
        vm.editPlaylist = editPlaylist;
        vm.removeTopten = removeTopten;

        //  updates playlists
        function createPlaylist() {
            vm.toptens.playlists = toptenService.createPlaylist(vm.toptenTitle)
            $state.go('playlist', {iid: vm.toptens.playlists.length-1});
        }

        function editPlaylist(playlist, index) {
            console.log('$index', index)
            $state.go('playlist', {iid: index});
        }

        function removeTopten(index) {
            console.log('$index', index)
            vm.toptens.playlists = toptenService.removePlaylist(index);
        }

    };
})();
