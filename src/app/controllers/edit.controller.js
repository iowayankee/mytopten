(function () {
	'use strict';

	angular
		.module('topTenApp')
		.controller('editController', editController);

	editController.$inject = ['toptenService', '$state', '$stateParams'];

	function editController(toptenService, $state, $stateParams) {

		let vm = this;

		vm.searchTerm = "";
		vm.searchType = "track";
		vm.searchResults = {};
		vm.album = {};

		vm.searchTunes = searchTunes;
		vm.getAlbums = getAlbums;
		vm.getTracks = getTracks;
		vm.viewPlaylist = viewPlaylist;
		vm.addTrack = addTrack;
		vm.removeTrack = removeTrack;
		vm.downloadJson = downloadJson;

		initPlaylist();

		//	Search tunes via API
		function searchTunes() {

			toptenService.searchTunes(vm.searchTerm, vm.searchType)
				.then(function(data) {
					vm.showSection = vm.searchType
					vm.searchResults = data[vm.searchType + "s"].items;
				}, function(error) {
					console.log('error', error);
				});

		}

		//	select artist and get artist albums
		function getAlbums(name) {

			toptenService.searchTunes(name, 'album')
				.then(function(data) {
					vm.showSection = 'album';
					vm.searchResults = data["albums"].items;
				}, function(error) {
					console.log('error', error);
				});

		}

		function getTracks(albumurl) {
			vm.album = {};
			toptenService.getTracks(albumurl)
				.then(function(data) {
					vm.showSection = 'albumtracks'
					vm.album = data;
				}, function(error) {
					console.log('error', error);
				});

		}
		function viewPlaylist() {
			vm.showSection = "playlist";
		}

		function addTrack(track, href) {
			var playlists = toptenService.addToPlaylist($stateParams.iid, track, href)
			vm.playlist = playlists[$stateParams.iid];
			if(vm.playlist.tracks.length >= 10) {
				vm.showSection = "playlist";
			}

		}
		function removeTrack(index) {
			var playlist = toptenService.removeFromPlaylist($stateParams.iid, index)
			vm.playlist.tracks = playlist;
		}

		function downloadJson() {
			var a = $('<a/>', {
				style:'display:none',
				href:'data:application/octet-stream;base64,'+btoa(vm.playlist),
				download:'emailStatistics.csv'
			}).appendTo('body')

		}

		function initPlaylist() {
			vm.showSection = "playlist";

			if(!$stateParams.iid) {
				$state.go('/');
			} else {
				var playlists = toptenService.getPlaylist($stateParams.iid)
				if(!playlists.title) $state.go('/');
				vm.playlist = playlists;
			}
		}

	};
})();
