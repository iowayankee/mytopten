(function () {
    'use strict';

    angular
        .module('topTenApp', [
            'ui.router'
        ])

        .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('playlist', {
                url: '/playlist/:iid',
                params: {
                    iid: null
                },
                templateUrl: '/views/editPlaylist.html',
                controllerAs: 'editCtrl',
                controller: 'editController'
            })
            .state('/', {
                url: '/',
                templateUrl: '/views/createTopTen.html',
                controllerAs: 'createCtrl',
                controller: 'createController'
            });

        $locationProvider.html5Mode(true);

    });
})();
