(function () {
	'use strict';

	angular
		.module('topTenApp')
		.directive('addTopTen', addTopTen)

	addTopTen.$inject = ['$state']

	function addTopTen($state) {
		return {
			restrict: 'E',
			templateUrl: 'components/addTopTen/addTopTen.template.html',
			link: function (scope, elem, attrs) {
				var vm = scope;


				// gg.dstate = {dash: true, games: false, history: false, victories: false}
				// gg.dstate = 'dash'
				//
				// gg.gotoDashState = function(state) {
				// 	gg.dstate = state
				// }
				// gg.gotoCheckin = function() {
				// 	console.log('gotoCheckin scope.gameData', gg.game)
				// 	gg.game.inplay = false
				// 	gg.gotoSection('checkin', {gid: gg.game.gid, ipid: null});
				// }
				//
				// gg.refreshChecker('game')

				console.log('addTopTen directive')

			}
			//controllerAs: dashboardController,
		}

	}
})();

asdf<section id="main" ng-show="todoCtrl.todos.length" ng-cloak>
 <input id="toggle-all" type="checkbox" ng-model="todoCtrl.allChecked" ng-click="todoCtrl.markAll(todoCtrl.allChecked)">
 <label for="toggle-all">Mark all as complete</label>
 <ul id="todo-list">
  <li ng-repeat="todo in todoCtrl.todos | filter:statusFilter track by $index" ng-class="{completed: todo.completed, editing: todo == todoCtrl.editedTodo}">
   <div class="view">
    <input class="toggle" type="checkbox" ng-model="todo.completed" ng-change="todoCtrl.toggleCompleted(todo)">
    <label ng-dblclick="todoCtrl.editTodo(todo)">{{todo.title}}</label>
    <button class="destroy" ng-click="todoCtrl.removeTodo(todo)"></button>
   </div>
   <form ng-submit="todoCtrl.saveEdits(todo, 'submit')">
    <input class="edit" ng-trim="false" ng-model="todo.title" todo-escape="todoCtrl.revertEdits(todo)" ng-blur="todoCtrl.saveEdits(todo, 'blur')" todo-focus="todo == todoCtrl.editedTodo">
   </form>
  </li>
 </ul>
</section>

(function () {
    'use strict';

    angular
        .module('topTenApp')
        .controller('todoController', ['$scope', '$filter', function ($scope, $filter) {
            'use strict';

            var todoList = this;

            // Use local storage.
            var todos = todoList.todos = [
                {
                    title: 'Express 4X',
                    completed: false
                },
                {
                    title: 'Angular 1.3',
                    completed: false
                }
            ];

            todoList.newTodo = '';
            todoList.editedTodo = null;

            $scope.$watch(function () {
                return todos;
            }, function () {
                todoList.remainingCount = $filter('filter')(todos, {completed: false}).length;
                todoList.completedCount = todos.length - todoList.remainingCount;
                todoList.allChecked = !todoList.remainingCount;
            }, true);

            todoList.addTodo = function () {
                var newTodo = {
                    title: todoList.newTodo.trim(),
                    completed: false
                };
                if (!newTodo.title) {
                    return;
                }

                todos.push(newTodo);
                todoList.newTodo = '';
            };

            // Make css style effective.
            todoList.editTodo = function (todo) {
                todoList.editedTodo = todo;
                // Clone the original todo to restore it on demand.
                todoList.originalTodo = angular.extend({}, todo);
            };

            todoList.saveEdits = function (todo, event) {
                // Blur events are automatically triggered after the form submit event.
                // This does some unfortunate logic handling to prevent saving twice.
                if (event === 'blur' && todoList.saveEvent === 'submit') {
                    todoList.saveEvent = null;
                    return;
                }
                todoList.saveEvent = event;
                if (todoList.reverted) {
                    // Todo edits were reverted-- don't save.
                    todoList.reverted = null;
                    return;
                }
                todo.title = todo.title.trim();
                if (todo.title === todoList.originalTodo.title) {
                    todoList.editedTodo = null;
                    return;
                }

                todos[todos.indexOf(todoList.originalTodo)] = todo;

                todoList.editedTodo = null;
            };

            todoList.revertEdits = function (todo) {
                todos[todos.indexOf(todo)] = todoList.originalTodo;
                todoList.editedTodo = null;
                todoList.originalTodo = null;
                todoList.reverted = true;
            };

            todoList.removeTodo = function (todo) {
                todos.splice(todos.indexOf(todo), 1);
            };

            todoList.toggleCompleted = function (todo, completed) {
                if (angular.isDefined(completed)) {
                    todo.completed = completed;
                }
            };

            todoList.markAll = function (completed) {
                todos.forEach(function (todo) {
                    if (todo.completed !== completed) {
                        todoList.toggleCompleted(todo, completed);
                    }
                });
            };
        }]);
})();
